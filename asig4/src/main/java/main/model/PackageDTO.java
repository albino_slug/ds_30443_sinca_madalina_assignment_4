package main.model;


import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
public class PackageDTO {

    public static PackageDTO fromPackage(Package aPackage){
        PackageDTO packageDTO = PackageDTO.builder()
                .id(aPackage.getId())
                .name(aPackage.getName())
                .description(aPackage.getDescription())
                .sender(aPackage.getSender())
                .receiver(aPackage.getReceiver())
                .destinationCity(aPackage.getDestinationCity())
                .senderCity(aPackage.getSenderCity())
                .build();

        for(RoutePoint routePoint : aPackage.getTrackingInfo()){
            packageDTO.trackingInfo.add(routePoint.getId());
        }

        return packageDTO;
    }

    private Integer id;

    private String name;

    private String description;

    private boolean tracked;

    private User sender;

    private User receiver;

    private City destinationCity;

    private City senderCity;

    @Builder.Default
    private ArrayList<Integer> trackingInfo = new ArrayList<>();

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ",\n name='" + name + '\'' +
                ",\n description='" + description + '\'' +
                ",\n tracked=" + tracked +
                ",\n sender=" + sender +
                ",\n receiver=" + receiver +
                ",\n destinationCity=" + destinationCity +
                ",\n senderCity=" + senderCity +
                '}';
    }
}