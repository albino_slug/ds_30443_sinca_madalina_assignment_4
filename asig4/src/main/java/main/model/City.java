package main.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
//@ToString
@Entity
@Table(name = "city")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "lat")
    private long lat;

    @Column(name = "lng")
    private long lng;

//    @OneToMany(mappedBy = "destinationCity")
//    private List<Package> receivedPackageList = new ArrayList<>();
//
//    @OneToMany(mappedBy = "senderCity")
//    private List<Package> sentPackageList = new ArrayList<>();
}
