package main.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@ToString
@Entity
@Table(name = "package")
public class Package {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "tracked")
    private boolean tracked;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "package_sender", referencedColumnName = "id")
    private User sender;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "package_receiver", referencedColumnName = "id")
    private User receiver;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "package_destination_city", referencedColumnName = "id")
    private City destinationCity;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "package_origin_city", referencedColumnName = "id")
    private City senderCity;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "aPackage", cascade = CascadeType.ALL)
    //@JoinColumn(name = "tracking_info", referencedColumnName = "id")
    @Builder.Default
    private List<RoutePoint> trackingInfo = new ArrayList<>();

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", tracked=" + tracked +
                ", sender=" + sender +
                ", receiver=" + receiver +
                ", destinationCity=" + destinationCity +
                ", senderCity=" + senderCity +
                '}';
    }
}
