package main.service;

import main.model.*;
import main.model.Package;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

//Service Endpoint Interface
@WebService
@Service
@Configurable
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface AdminService {
    @WebMethod String getHelloWorldAsString(String name);
    @WebMethod String addPackage(Package aPackage);
    @WebMethod String removePackageById(Integer packageId);
    @WebMethod Package getPackageById(Integer packageId);
    @WebMethod String modifyPackageTrackingStatus(Integer packageId, Boolean tracking);
    @WebMethod String updatePackageStatus(Integer packageId, Date date, String cityName);
    @WebMethod ArrayList<RoutePoint> getAllRoutePoints(Integer packageId);
    @WebMethod Package constructPackageFromFields(String senderID, String receiverID,
                                       String name, String description, Boolean tracked,
                                       String destinationCity, String originCity);
}
