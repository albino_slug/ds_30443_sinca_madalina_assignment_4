package main.service;

import main.model.PackageDTO;
import main.model.User;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

//Service Endpoint Interface
@WebService
@Service
@Configurable
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface UserService {
    @WebMethod String getHelloWorldAsString(String name);
    @WebMethod PackageDTO[] getAllUserPackages(Integer userId);
    @WebMethod User login(String username, String password);
    @WebMethod String register(String username, String password);
    @WebMethod String getPackageStatus(int packageId);
    @WebMethod PackageDTO searchPackages(int userId, String packageName);
}
