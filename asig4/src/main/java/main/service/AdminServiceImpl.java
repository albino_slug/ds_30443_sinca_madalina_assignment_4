package main.service;

import lombok.AllArgsConstructor;
import main.model.RoutePoint;
import main.model.Package;
import main.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;


import javax.jws.WebService;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

//@Service
//@Configurable
@WebService(endpointInterface = "main.service.AdminService")
public class AdminServiceImpl implements AdminService {
//    @Autowired
    private PackageRepository packageRepository;

//    @Autowired
    private RoutePointRepository routePointRepository;

    //    @Autowired
    private UserRepository userRepository;

    static private CityRepository cityRepository;

    public AdminServiceImpl() {
        this(new PackageRepositoryImpl(), new RoutePointRepositoryImpl(), new UserRepositoryImpl(), new CityRepositoryImpl());
    }

    public AdminServiceImpl(PackageRepository packageRepository, RoutePointRepository routePointRepository, UserRepository userRepository, CityRepository cityRepository) {
        this.packageRepository = packageRepository;
        this.routePointRepository = routePointRepository;
        this.userRepository = userRepository;
        this.cityRepository = cityRepository;
    }

    @Override
    public String getHelloWorldAsString(String name) {
        return "hello " + name;
    }

    @Override
    public String removePackageById(Integer packageId) {
        packageRepository.deleteById(packageId);
        return "success";
    }

    @Override
    public Package getPackageById(Integer packageId) {
        if (packageRepository.findById(packageId).isPresent()){
            return packageRepository.findById(packageId).get();
        }
        return new Package();
    }

    @Override
    public String modifyPackageTrackingStatus(Integer packageId, Boolean tracking) {
        Optional<Package> optionalPackage = packageRepository.findById(packageId);
        if (optionalPackage.isPresent()){
            Package aPackage = optionalPackage.get();
            aPackage.setTracked(tracking);
            packageRepository.save(aPackage);
            return "success";
        }
        return "fail";
    }

    @Override
    public String updatePackageStatus(Integer packageId, Date date, String cityName) {
        Optional<Package> optionalPackage = packageRepository.findById(packageId);
        RoutePoint routePoint;

        if (optionalPackage.isPresent()){
            Package anotherPackage = optionalPackage.get();

            if (cityRepository.findByName(cityName).isPresent()){
                routePoint = RoutePoint.builder()
                        .city(cityRepository.findByName(cityName).get())
                        .date(date)
                        .aPackage(anotherPackage)
                        .build();
            }
            else {
                return "fail";
            }

            if (anotherPackage.isTracked()) {
                routePoint = addRoutePoint(routePoint);
                anotherPackage.getTrackingInfo().add(routePoint);
                packageRepository.save(anotherPackage);
                return "success";
            }
            else{
                return "package not traceable";
            }
        }
        return "fail";
    }

    @Override
    public ArrayList<RoutePoint> getAllRoutePoints(Integer packageId) {
        Optional<Package> optionalPackage = packageRepository.findById(packageId);
        if (optionalPackage.isPresent()) {
            return (ArrayList<RoutePoint>) optionalPackage.get().getTrackingInfo();
        }
        return new ArrayList<>();
    }

    @Override
    public String addPackage(Package aPackage) {
        packageRepository.save(aPackage);
        return "success";
    }

    public RoutePoint addRoutePoint(RoutePoint routePoint) {
        return routePointRepository.save(routePoint);
    }

    public Package constructPackageFromFields(String senderID, String receiverID,
                                               String name, String description, Boolean tracked,
                                               String destinationCity, String originCity){
        int senderId = Integer.parseInt(senderID);
        int receiverId = Integer.parseInt(receiverID);
        if (cityRepository.findByName(originCity).isPresent()
                && cityRepository.findByName(destinationCity).isPresent()
                && userRepository.getById(senderId).isPresent()
                && userRepository.getById(receiverId).isPresent()){

            Package thePackage = Package.builder()
                    .name(name)
                    .description(description)
                    .tracked(tracked)
                    .sender(userRepository.getById(senderId).get())
                    .receiver((userRepository.getById(receiverId).get()))
                    .senderCity(cityRepository.findByName(originCity).get())
                    .destinationCity(cityRepository.findByName(destinationCity).get())
                    .build();

            return thePackage;
        }
        else {
            return null;
        }
    }

}
