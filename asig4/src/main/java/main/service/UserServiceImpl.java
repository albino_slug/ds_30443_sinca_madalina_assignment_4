package main.service;

import com.mysql.cj.x.protobuf.MysqlxExpr;
import main.model.*;
import main.model.Package;
import main.repository.PackageRepository;
import main.repository.PackageRepositoryImpl;
import main.repository.UserRepository;
import main.repository.UserRepositoryImpl;
import org.springframework.ws.server.endpoint.annotation.Endpoint;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Optional;

@Endpoint
//@Service
//@Configurable
@WebService(endpointInterface = "main.service.UserService")
public class UserServiceImpl implements UserService {
//    @Autowired
    private PackageRepository packageRepository;

//    @Autowired
    private UserRepository userRepository;

    public UserServiceImpl(){
        this(new PackageRepositoryImpl(), new UserRepositoryImpl());
    }

    public UserServiceImpl(PackageRepository packageRepository, UserRepository userRepository) {
        this.packageRepository = packageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public PackageDTO searchPackages(int userId, String packageName){
        if (userRepository.getById(userId).isPresent()){
            PackageDTO[] packageDTOS = getAllUserPackages(userId);
            for (PackageDTO packageDTO : packageDTOS){
                if (packageDTO.getName().equals(packageName)){
                    return packageDTO;
                }
            }
        }
        return null;
    }

    @Override
    public String getPackageStatus(int packageId) {
        Package aPackage;
        String routePointString;
        if (packageRepository.findById(packageId).isPresent()) {
            aPackage = packageRepository.findById(packageId).get();

            if (aPackage.getTrackingInfo().isEmpty() == false) {
                RoutePoint routePoint = aPackage.getTrackingInfo().get(aPackage.getTrackingInfo().size() - 1);
                routePointString = "\nId=" + routePoint.getId() + "\nDate=" + routePoint.getDate() + "\nCity=" + routePoint.getCity().getName();

                return routePointString;
            }

        }
        return null;
    }

    @Override
    public PackageDTO[] getAllUserPackages(Integer userId) {
        ArrayList<Package> packages = packageRepository.findPackagesByReceiver(userRepository.getById(userId).get());
        ArrayList<PackageDTO> packageDTOS = new ArrayList<PackageDTO>();

        for(Package aPackage : packages){
            PackageDTO packageDTO = PackageDTO.fromPackage(aPackage);
            packageDTOS.add(packageDTO);
        }

        PackageDTO[] packageDTOSlist = packageDTOS.toArray(new PackageDTO[packageDTOS.size()]);
        return packageDTOSlist;
    }

    @Override
    public String getHelloWorldAsString(String name) {
        return "hello " + name;
    }

    @Override
    public User login(String username, String password) {
        Optional<User> user = userRepository.getByUsernameAndAndPassword(username, password);
        if (user.isPresent()){
            return user.get();
        }
        else{
            return null;
        }
    }

    @Override
    public String register(String username, String password) {
        if (userRepository.getByUsername(username).isPresent()){
            return "this username is already present";
        }
        else{
            User newUser = User.builder().password(password).role(UserRole.USER).username(username).build();
            userRepository.save(newUser);
            return "success";
        }
    }
}
