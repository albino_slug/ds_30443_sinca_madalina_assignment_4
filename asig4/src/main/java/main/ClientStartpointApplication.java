package main;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import main.model.User;
import main.model.UserRole;
import main.service.AdminService;
import main.service.UserService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ClientStartpointApplication extends Application {

    private AdminService adminService;
    private UserService userService;

    Stage stage;
    AdminUI adminUI;
    UserUI userUI;

    private void createConnection() throws MalformedURLException {
        URL adminUrl = new URL("http://localhost:7779/ws/admin?wsdl");
        URL userUrl = new URL("http://localhost:7779/ws/user?wsdl");

        QName adminServiceQName = new QName("http://service.main/", "AdminServiceImplService");
        Service adminService = Service.create(adminUrl, adminServiceQName);
        this.adminService = adminService.getPort(AdminService.class);

        QName userServiceQName = new QName("http://service.main/", "UserServiceImplService");
        Service userService = Service.create(userUrl, userServiceQName);
        this.userService = userService.getPort(UserService.class);

        System.out.println(this.adminService.getHelloWorldAsString("Connection to admin service is working"));
        System.out.println(this.userService.getHelloWorldAsString("Connection to user service is working"));
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        createConnection();
        adminUI = new AdminUI(this.adminService);
        userUI = new UserUI(this.userService);

        primaryStage.setTitle("Registration Form JavaFX Application");
        this.stage = primaryStage;

        GridPane gridPane = createRegistrationFormPane();
        addUIControls(gridPane);
        Scene scene = new Scene(gridPane, 300, 200);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private GridPane createRegistrationFormPane() {

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        return gridPane;
    }

    private void addUIControls(GridPane gridPane) {
        // Add Header
        Label headerLabel = new Label("Registration Form");
        gridPane.add(headerLabel, 0,0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0,20,0));

        // Add Name Label
        Label nameLabel = new Label("username : ");
        gridPane.add(nameLabel, 0,1);

        // Add Name Text Field
        TextField nameField = new TextField();
        gridPane.add(nameField, 1,1);

        // Add Password Label
        Label passwordLabel = new Label("password : ");
        gridPane.add(passwordLabel, 0, 2);

        // Add Password Field
        PasswordField passwordField = new PasswordField();
        gridPane.add(passwordField, 1, 2);

        // Add Submit Button
        Button login = new Button("log in");
        login.setDefaultButton(true);
        gridPane.add(login, 0, 3);
        GridPane.setHalignment(login, HPos.RIGHT);
        GridPane.setMargin(login, new Insets(20, 0,20,0));

        Button signUp = new Button("sign up");
        signUp.setDefaultButton(true);
        gridPane.add(signUp, 1, 3);
        GridPane.setHalignment(signUp, HPos.LEFT);
        GridPane.setMargin(signUp, new Insets(20, 0,20,0));

        login.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(nameField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "please enter a username");
                    return;
                }
                if(passwordField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "please enter a password");
                    return;
                }

                User user = userService.login(nameField.getText(), passwordField.getText());
                if (user == null){
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "unregistered user or wrong credentials - please try again");
                    return;
                }

                if (user.getRole() == UserRole.USER){
                    userUI.start(stage);
                }
                else {
                    adminUI.start(stage);
                }
            }
        });

        signUp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(nameField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "please enter a username");
                    return;
                }
                if(passwordField.getText().isEmpty()) {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "please enter a password");
                    return;
                }

                String outcome = userService.register(nameField.getText(), passwordField.getText());
                if (outcome == "this username is already present"){
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "error", "it seems you already have an account - please log in");
                    return;
                }

                User user = userService.login(nameField.getText(), passwordField.getText());
                userUI.start(stage);
            }
        });
    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
