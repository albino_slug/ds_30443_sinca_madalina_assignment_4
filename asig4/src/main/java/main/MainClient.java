package main;

import main.service.AdminService;
import main.service.UserService;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

//@SpringBootApplication
public class MainClient {
    private AdminService adminService;
    private UserService userService;

    public static void main(String[] args) throws Exception {
      MainClient mainClient = new MainClient();
      //mainClient.initUI();
    }

    private void initUI() {}

    public MainClient() throws MalformedURLException {
        URL adminUrl = new URL("http://localhost:7779/ws/admin?wsdl");
        URL userUrl = new URL("http://localhost:7779/ws/user?wsdl");

        QName adminServiceQName = new QName("http://service.main/", "AdminServiceImplService");
        Service adminService = Service.create(adminUrl, adminServiceQName);
        this.adminService = adminService.getPort(AdminService.class);

        QName userServiceQName = new QName("http://service.main/", "UserServiceImplService");
        Service userService = Service.create(userUrl, userServiceQName);
        this.userService = userService.getPort(UserService.class);

        System.out.println(this.adminService.getHelloWorldAsString("Connection to admin service is working"));
        System.out.println(this.userService.getHelloWorldAsString("Connection to user service is working"));
    }
}
