package main;

import main.repository.*;
import main.service.AdminServiceImpl;
import main.service.UserServiceImpl;

import javax.xml.ws.Endpoint;

//@SpringBootApplication
public class MainServicePublisher{

    static private PackageRepository packageRepository = new PackageRepositoryImpl();
    static private UserRepository userRepository = new UserRepositoryImpl();
    static private RoutePointRepository routePointRepository = new RoutePointRepositoryImpl();
    static private CityRepository cityRepository = new CityRepositoryImpl();

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:7779/ws/admin", new AdminServiceImpl(packageRepository, routePointRepository, userRepository, cityRepository));
        Endpoint.publish("http://localhost:7779/ws/user", new UserServiceImpl(packageRepository, userRepository));
    }
}
