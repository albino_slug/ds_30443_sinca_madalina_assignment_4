package main.repository;

import main.HibernateUtil;
import main.model.User;

import java.util.List;
import java.util.Optional;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import javax.transaction.Transactional;

public class UserRepositoryImpl implements UserRepository {
    @Override
    public Optional<User> getByUsernameAndAndPassword(String username, String password) {
        Optional<User> user = getByUsername(username);
        if (user.isPresent() && (user.get().getPassword().equals(password))){
            return user;
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> getByUsername(String username) {
        Session session = null;
        Optional<User> user;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            user = Optional.of((User) session.createCriteria(User.class,"user")
                    .add(Restrictions.eq("user.username",username)).uniqueResult());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return user;
    }

    @Override
    public Optional<User> getById(Integer id) {
        Session session = null;
        Optional<User>  user = Optional.empty();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            user = Optional.of(session.get(User.class, id));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            return session.createQuery("from User", User.class).list();
        }
    }


    @Override
    public User save(User user) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return user;
    }
}
