package main.repository;

import main.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

public interface UserRepository{
    Optional<User> getByUsernameAndAndPassword(String username, String password);
    Optional<User> getByUsername(String username);
    List<User> findAll();
    Optional<User> getById(Integer id);
    User save(User user);
}
