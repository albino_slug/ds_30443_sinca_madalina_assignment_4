package main.repository;

import main.HibernateUtil;
import main.model.Package;
import main.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import javax.management.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PackageRepositoryImpl implements PackageRepository{
    @Override
    public ArrayList<Package> findPackagesByReceiver(User receiver) {
        Session session = null;
        ArrayList<Package> packages = new ArrayList<Package>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            packages = new ArrayList<Package>(session.createCriteria(Package.class,"package")
                    .add(Restrictions.eq("package.receiver.id",receiver.getId())).list());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return packages;
    }

    @Override
    public void deleteById(Integer packageId) {
        Session session = null;
        Transaction transaction = null;
        Package aPackage;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            if (session.get(Package.class, packageId) != null){
                transaction = session.beginTransaction();
                aPackage = session.get(Package.class, packageId);
                session.delete(aPackage);
                session.flush() ;
                transaction.commit();
            }
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public Optional<Package> findById(Integer packageId) {
        Session session = null;
        Optional<Package> aPackage = Optional.empty();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            aPackage = Optional.of(session.get(Package.class, packageId));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return aPackage;
    }

    @Override
    public Package save(Package aPackage) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.saveOrUpdate(aPackage);
          transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return aPackage;
    }
}
