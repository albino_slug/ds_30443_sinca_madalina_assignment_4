package main.repository;

import main.model.RoutePoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface RoutePointRepository {
    RoutePoint save(RoutePoint routePoint);
    Optional<RoutePoint> getById(Integer id);
}
