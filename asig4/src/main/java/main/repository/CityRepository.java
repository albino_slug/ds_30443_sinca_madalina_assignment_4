package main.repository;

import main.model.City;

import java.util.Optional;

public interface CityRepository {
    Optional<City> findById(Integer id);
    Optional<City> findByName(String username);
}
