package main.repository;

import main.HibernateUtil;
import main.model.City;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Optional;

public class CityRepositoryImpl implements CityRepository {

    @Override
    public Optional<City> findById(Integer id){
        Session session = null;
        Optional<City> city = Optional.empty();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            if (session.get(City.class, id) != null){
                city = Optional.of(session.get(City.class, id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return city;
    }

    @Override
    public Optional<City> findByName(String name) {
        Session session = null;
        Optional<City> city;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            city = Optional.of((City) session.createCriteria(City.class,"city")
                    .add(Restrictions.eq("city.name", name)).uniqueResult());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return city;
    }
}
