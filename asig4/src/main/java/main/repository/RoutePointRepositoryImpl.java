package main.repository;

import main.HibernateUtil;
import main.model.RoutePoint;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

public class RoutePointRepositoryImpl implements RoutePointRepository {
    @Override
    public RoutePoint save(RoutePoint routePoint) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            transaction = session.beginTransaction();
            session.save(routePoint);
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return routePoint;
    }

    @Override
    public Optional<RoutePoint> getById(Integer id) {
        Session session = null;
        Optional<RoutePoint> routePoint = Optional.empty();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            routePoint = Optional.of(session.get(RoutePoint.class, id));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return routePoint;
    }
}
