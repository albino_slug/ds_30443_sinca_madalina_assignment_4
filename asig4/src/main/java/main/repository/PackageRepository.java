package main.repository;

import main.model.Package;
import main.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface PackageRepository {
    ArrayList<Package> findPackagesByReceiver(User receiver);
    void deleteById(Integer id);
    Optional<Package> findById(Integer packageId);
    Package save(Package aPackage);
}
