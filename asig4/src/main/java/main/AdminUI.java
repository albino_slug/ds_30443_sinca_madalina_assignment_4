package main;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.model.Package;
import main.model.RoutePoint;
import main.service.AdminService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class AdminUI extends Application {
    private AdminService adminService;

    public AdminUI(AdminService adminService){
        this.adminService = adminService;
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void removePackage(int packageId){
        adminService.removePackageById(packageId);
    }

    private Date getDateFromLocalDate(LocalDate date){
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Admin Application");

        // TEXT FIELDS
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);

        final TextField id = new TextField();
        id.setPromptText("enter the package ID");
        id.setPrefColumnCount(10);
        id.getText();
        GridPane.setConstraints(id, 0, 0);
        grid.getChildren().add(id);

        final TextField sender = new TextField();
        sender.setPromptText("enter sender ID");
        GridPane.setConstraints(sender, 0, 1);
        grid.getChildren().add(sender);

        final TextField receiver = new TextField();
        receiver.setPrefColumnCount(15);
        receiver.setPromptText("enter receiver ID");
        GridPane.setConstraints(receiver, 0, 2);
        grid.getChildren().add(receiver);

        final TextField name = new TextField();
        name.setPromptText("enter package name");
        name.setPrefColumnCount(10);
        name.getText();
        GridPane.setConstraints(name, 0, 3);
        grid.getChildren().add(name);

        final TextField description = new TextField();
        description.setPromptText("enter package description");
        GridPane.setConstraints(description, 0, 4);
        grid.getChildren().add(description);

        final ChoiceBox origin = new ChoiceBox(FXCollections.observableArrayList(
                "Barcelona", "Bucharest", "Kiev",
                "Lyon", "Prague", "Copenhagen")
        );
        origin.setPrefWidth(230);
        GridPane.setConstraints(origin, 0, 5);
        grid.getChildren().add(origin);
        origin.setTooltip(new Tooltip("select the package origin city"));

        final ChoiceBox destination = new ChoiceBox(FXCollections.observableArrayList(
                "Barcelona", "Bucharest", "Kiev",
                "Lyon", "Prague", "Copenhagen")
        );
        destination.setPrefWidth(230);
        GridPane.setConstraints(destination, 0, 6);
        grid.getChildren().add(destination);
        destination.setTooltip(new Tooltip("select the package destination city"));

        // DATE PICKER
        final DatePicker datePicker = new DatePicker();
        datePicker.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                LocalDate date = datePicker.getValue();
                System.out.println("Selected date: " + date);
            }
        });
        GridPane.setConstraints(datePicker, 0, 10);
        grid.getChildren().add(datePicker);

        final ChoiceBox currentCity = new ChoiceBox(FXCollections.observableArrayList(
                "Barcelona", "Bucharest", "Kiev",
                "Lyon", "Prague", "Copenhagen")
        );
        currentCity.setPrefWidth(230);
        GridPane.setConstraints(currentCity, 0, 11);
        grid.getChildren().add(currentCity);
        currentCity.setTooltip(new Tooltip("select the package current city"));

        CheckBox tracking = new CheckBox("package tracking");
        tracking.setIndeterminate(false);
        GridPane.setConstraints(tracking, 0, 8);
        grid.getChildren().add(tracking);

        // ADD A NEW PACKAGE
        Button addPackage = new Button();
        addPackage.setText("Add A New Package");
        addPackage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(sender.getText() != null
                && receiver.getText() != null && name.getText() != null
                && description.getText() != null && origin.getValue() != null
                && destination.getValue() != null){
                    Boolean tracked = Boolean.FALSE;
                    if (tracking.isSelected()){
                        tracked = Boolean.TRUE;
                    }
                    Package thePackage = adminService.constructPackageFromFields(sender.getText(), receiver.getText(),
                            name.getText(), description.getText(), tracked,
                            destination.getValue().toString(), origin.getValue().toString());

                    if (thePackage != null){
                        adminService.addPackage(thePackage);

                        Text successText = new Text (10, 20, "The Package has been successfully added!");
                        GridPane.setConstraints(successText, 0, 9);
                        grid.getChildren().add(successText);
                    }
                    else {
                        Text errorText = new Text (10, 20, "Unknown cities or users!");
                        GridPane.setConstraints(errorText, 0, 9);
                        grid.getChildren().add(errorText);
                    }
                }
                else{
                    Text errorText = new Text (10, 20, "Make sure you filled in all the fields!");
                    GridPane.setConstraints(errorText, 0, 9);
                    grid.getChildren().add(errorText);
                }
            }
        });
        GridPane.setConstraints(addPackage, 1, 0);
        grid.getChildren().add(addPackage);

        // GET ALL ROUTE POINTS OF A PACKAGE
        Button getRoutePoints = new Button();
        getRoutePoints.setText("Get All Route Points");
        getRoutePoints.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(id.getText() != null){
                    ArrayList<RoutePoint> routePoints = adminService.getAllRoutePoints(Integer.parseInt(id.getText()));

                    int iterator = 9;
                    for (RoutePoint routePoint : routePoints){
                        Text errorText = new Text (10, 20, routePoint.getId().toString()
                                + routePoint.getDate().toString()
                                + routePoint.getCity().toString());
                        GridPane.setConstraints(errorText, 0, iterator);
                        grid.getChildren().add(errorText);
                    }
                }
                else {
                    Text errorText = new Text (10, 20, "Make sure you selected an ID!");
                    GridPane.setConstraints(errorText, 0, 9);
                    grid.getChildren().add(errorText);
                }
            }
        });
        GridPane.setConstraints(getRoutePoints, 1, 1);
        grid.getChildren().add(getRoutePoints);

        // UPDATE PACKAGE STATUS / ADD NEW ROUTE POINT
        Button updatePackage = new Button();
        updatePackage.setText("Update Package Status");
        updatePackage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(id.getText() != null && datePicker.getValue() != null && currentCity.getValue() != null){
                    adminService.updatePackageStatus(Integer.parseInt(id.getText()),
                            getDateFromLocalDate(datePicker.getValue()),
                            currentCity.getValue().toString());

                    Text successText = new Text (10, 20, "A new Route Point has been added to the package!");
                    GridPane.setConstraints(successText, 0, 9);
                    grid.getChildren().add(successText);
                }
                else {
                    Text errorText = new Text (10, 20, "Make sure you selected a date, ID and current city!");
                    GridPane.setConstraints(errorText, 0, 9);
                    grid.getChildren().add(errorText);
                }
            }
        });
        GridPane.setConstraints(updatePackage, 1, 2);
        grid.getChildren().add(updatePackage);

        // REGISTER PACKAGE FOR TRACKING
        Button makeTrackable = new Button();
        makeTrackable.setText("Register For Tracking");
        makeTrackable.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Boolean tracked = Boolean.FALSE;
                if (tracking.isSelected()){
                    tracked = Boolean.TRUE;
                }
                if (id.getText() != null){
                    adminService.modifyPackageTrackingStatus(Integer.parseInt(id.getText()), tracked);

                    Text successText = new Text (10, 20, "The Package tracking status has been successfully updated!");
                    GridPane.setConstraints(successText, 0, 9);
                    grid.getChildren().add(successText);
                }
                else {
                    Text errorText = new Text (10, 20, "Make sure you filled in the ID field!");
                    GridPane.setConstraints(errorText, 0, 9);
                    grid.getChildren().add(errorText);
                }
            }
        });
        GridPane.setConstraints(makeTrackable, 1, 3);
        grid.getChildren().add(makeTrackable);

        // REMOVE PACKAGE
        Button removePackage = new Button();
        removePackage.setText("Remove Package");
        removePackage.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (id.getText() != null){
                    removePackage(Integer.parseInt(id.getText()));

                    Text successText = new Text (10, 20, "The Package has been successfully removed!");
                    GridPane.setConstraints(successText, 0, 9);
                    grid.getChildren().add(successText);
                }
                else {
                    Text errorText = new Text (10, 20, "Make sure you filled in the ID field!");
                    GridPane.setConstraints(errorText, 0, 9);
                    grid.getChildren().add(errorText);
                }
            }
        });
        GridPane.setConstraints(removePackage, 1, 4);
        grid.getChildren().add(removePackage);

        primaryStage.setScene(new Scene(grid, 410, 500));
        primaryStage.show();
    }
}
