package main;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.model.PackageDTO;
import main.model.RoutePoint;
import main.service.UserService;
import main.model.Package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserUI extends Application {
    private UserService userService;

    public UserUI(UserService userService){
        this.userService = userService;
    }

//    public static void main(String[] args) {
//        launch(args);
//    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("User Application");

        // TEXT FIELDS
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(5);
        grid.setHgap(5);

        final TextField uid = new TextField();
        uid.setPromptText("enter the user ID");
        uid.setPrefColumnCount(10);
        uid.getText();
        GridPane.setConstraints(uid, 0, 0);
        grid.getChildren().add(uid);

        final TextField id = new TextField();
        id.setPromptText("enter the package ID");
        id.setPrefColumnCount(10);
        id.getText();
        GridPane.setConstraints(id, 0, 1);
        grid.getChildren().add(id);

        final TextField name = new TextField();
        name.setPromptText("enter package name");
        name.setPrefColumnCount(10);
        name.getText();
        GridPane.setConstraints(name, 0, 2);
        grid.getChildren().add(name);

        // VIEW ALL PACKAGES
        Button getAllPackages = new Button();
        getAllPackages.setText("View all my packages");
        getAllPackages.setOnAction(event -> {
            if (uid.getText() != null){
                PackageDTO[] packageDTOS = userService.getAllUserPackages(Integer.parseInt(uid.getText()));

                ListView<String> list = new ListView<String>();
                ObservableList<String> items = FXCollections.observableArrayList();

                for (PackageDTO packageDTO : packageDTOS){
                    items.add("ID = " + packageDTO.getId() + ", Name = " + packageDTO.getName());
                }

                list.setItems(items);

                list.setPrefHeight(150);
                GridPane.setConstraints(list, 0, 3);
                grid.getChildren().add(list);
            }
            else {
                Text errorText = new Text (10, 20, "Make sure you selected a package ID!");
                GridPane.setConstraints(errorText, 0, 9);
                grid.getChildren().add(errorText);
            }
        });
        GridPane.setConstraints(getAllPackages, 1, 0);
        grid.getChildren().add(getAllPackages);

        // VIEW PACKAGE STATUS
        Button viewPackageStatus = new Button();
        viewPackageStatus.setText("View package status");
        viewPackageStatus.setOnAction(event -> {
            if (id.getText() != null){
                String routePoint = userService.getPackageStatus(Integer.parseInt(id.getText()));
                if (routePoint != null) {
                    Text text = new Text (10, 20, "Package Status:\n" + routePoint);
                    GridPane.setConstraints(text, 0, 9);
                    grid.getChildren().add(text);
                }
            }
            else {
                Text errorText = new Text (10, 20, "Make sure you selected a user ID!");
                GridPane.setConstraints(errorText, 0, 9);
                grid.getChildren().add(errorText);
            }
        });
        GridPane.setConstraints(viewPackageStatus, 1, 1);
        grid.getChildren().add(viewPackageStatus);

        // Search Packages
        Button searchPackages = new Button();
        searchPackages.setText("Search all my packages");
        searchPackages.setOnAction(event -> {
            if (uid.getText() != null && name.getText() != null){
                PackageDTO packageDTO = userService.searchPackages(Integer.parseInt(uid.getText()), name.getText());
                if (packageDTO != null){
                    Text text = new Text (10, 20, packageDTO.toString());
                    GridPane.setConstraints(text, 0, 10);
                    grid.getChildren().add(text);
                }
            }
            else {
                Text errorText = new Text (10, 20, "Make sure you selected a package name!");
                GridPane.setConstraints(errorText, 0, 9);
                grid.getChildren().add(errorText);
            }
        });
        GridPane.setConstraints(searchPackages, 1, 2);
        grid.getChildren().add(searchPackages);

        primaryStage.setScene(new Scene(grid, 410, 500));
        primaryStage.show();
    }
}
